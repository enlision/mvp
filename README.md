# Getting Started

This project was bootstrapped with [Vite](https://vitejs.dev/).

## Run following commands in your terminal to kick-start the project in DEV mode

1. `git clone https://github.com/uroslazarevic/mvp.git` -> clone project
2. `cd mvp` -> enter the newly cloned project
3. Make sure that port `5173` is the default port for the running application, as GitHub login functionality is relaying on it
4. `cp .env.example .env` => create `.env` file
5. `yarn` -> install all node_modules
6. `yarn dev` -> run the dev server

## Run the following command in your terminal to start the project in Docker environment

1. `yarn compose:up ` or `yarn compose:restart` -> Runs the app in the dev or production mode in Docker container.
2. Open [http://localhost:8080/](http://localhost:8080/) to view app in production mode in the browser.
3. Open [http://localhost:8081/](http://localhost:8081/) to view app in development mode in the browser.

## Available Scripts

In the project directory, you can run:

### `yarn dev`

Runs the app in the development mode.
Open [http://localhost:5173/](http://localhost:5173/) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `yarn test`

Runs the test suites in the project.

### `yarn build`

Builds the app for production in the `dist` folder.

### `yarn preview`

Runs the app in the production mode.
Open [http://localhost:4173/](http://localhost:4173/) to view it in the browser.

### `yarn compose:up ` or `yarn compose:restart`

Runs the app in the production mode in Docker container mapped to the port 8000.
Open [http://localhost:8080/](http://localhost:8080/) to view it in the browser.

### `yarn format`

Formats all source files with prettier.

### `yarn lint`

Lints all errors and warning in the project according to `.eslintrc.json` configuration file.

## Environment variables

- `VITE_SERVER_BASE_URL` => The server base URL for backend communication.
