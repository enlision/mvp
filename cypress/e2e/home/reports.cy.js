/// <reference types="cypress" />

describe('Reports screen', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('should navigate properly', () => {
    cy.get('aside img:nth-child(1)').click();
    cy.contains('First page');
    cy.get('aside img:nth-child(2)').click();
    cy.contains('Second page');
    cy.get('aside img:nth-child(3)').click();
    cy.contains('Third page');
    cy.get('aside img:nth-child(4)').click();
    cy.contains('Reports');
    cy.contains('Easily generate a report of your transactions');
    cy.get('aside img:nth-child(5)').click();
    cy.contains('Fifth page');
  });

  it('should display correct content', () => {
    cy.contains('h1', 'Reports');
    cy.contains('h2', 'Easily generate a report of your transactions');
    cy.contains('select', 'All projects');
    cy.contains('select', 'All gateways');
    cy.get('input[title="From date"]');
    cy.get('input[title="To date"]');
    cy.contains('button', 'Generate report');
    cy.contains('No reports');
    cy.contains(
      'Currently you have no data for the reports to be generated. Once you start generating traffic through the Balance application the reports will be shown.',
    );
    cy.get('img[alt="No Report Image"]');
    cy.get('footer').contains('Terms&Conditions | Privacy policy');
  });

  it('should generate Reports for All projects and All Gateways', () => {
    cy.get('select[name="select-project"]').select('All projects');
    cy.get('select[name="select-gateway"]').select('All gateways');
    cy.get('button').contains('Generate report').click();
    cy.contains('h3', 'All projects | All gateways');

    cy.get('ul li').should('have.length', 2);
    cy.get('ul li').first().contains('Project 1');
    cy.get('ul li').first().contains('TOTAL: 90989.170 USD');
    cy.get('ul li').first().find('tbody>tr').should('have.length', 50);

    cy.get('ul li:last-child').find('tbody>tr').should('have.length', 50);
    cy.get('ul li:last-child').contains('Project 2');
    cy.get('ul li:last-child').contains(/TOTAL: 99750.620 USD/i);

    cy.get('ul li').first().find('thead>tr').first().as('tableHeadRow');

    cy.get('@tableHeadRow').find('th:nth-child(1)').contains('Date');
    cy.get('@tableHeadRow').find('th:nth-child(2)').contains('Gateway');
    cy.get('@tableHeadRow').find('th:nth-child(3)').contains('Transaction ID');
    cy.get('@tableHeadRow').find('th:nth-child(4)').contains('Amount');
  });

  it('should generate Reports for Project 1 and All Gateways', () => {
    cy.get('select[name="select-project"]').select('Project 1');
    cy.get('select[name="select-gateway"]').select('All gateways');
    cy.get('button').contains('Generate report').click();
    cy.contains('h3', 'Project 1 | All gateways').as('tableTitle');

    cy.get('@tableTitle').next().find('li').should('have.length', 1);
    cy.get('@tableTitle').next().find('li').contains('Project 1');
    cy.get('@tableTitle').next().find('li').contains('TOTAL: 90989.170 USD');
    cy.get('@tableTitle')
      .next()
      .find('li')
      .find('tbody>tr')
      .should('have.length', 50);

    cy.get('@tableTitle')
      .next()
      .find('li')
      .find('thead>tr')
      .first()
      .as('tableHeadRow');

    cy.get('@tableHeadRow').find('th:nth-child(1)').contains('Date');
    cy.get('@tableHeadRow').find('th:nth-child(2)').contains('Gateway');
    cy.get('@tableHeadRow').find('th:nth-child(3)').contains('Transaction ID');
    cy.get('@tableHeadRow').find('th:nth-child(4)').contains('Amount');

    cy.get('div[data-testid="piechart"]').as('pieChart');
    cy.get('@pieChart').find('ul li').first().contains('Gateway 1');
    cy.get('@pieChart').find('ul li').last().contains('Gateway 2');
    cy.get('@pieChart').contains('34 %');
    cy.get('@pieChart').contains('66 %');
    cy.get('@pieChart')
      .find('p')
      .contains(/PROJECT TOTAL: 90989.170 USD/i);
  });

  it('should generate Reports for All Projects and Gateway 1', () => {
    cy.get('select[name="select-project"]').select('All projects');
    cy.get('select[name="select-gateway"]').select('Gateway 1');
    cy.get('button').contains('Generate report').click();
    cy.get('div[data-testid="table"]').as('table');
    cy.get('@table').contains('h3', 'All projects | Gateway 1');

    cy.get('@table').find('ul li:nth-child(1)').contains('Project 1');
    cy.get('@table')
      .find('ul li:nth-child(1)')
      .contains(/TOTAL: 59708.570 USD/);
    cy.get('@table').find('ul li:nth-child(2)').contains('Project 2');
    cy.get('@table')
      .find('ul li:nth-child(2)')
      .contains(/TOTAL: 63609.820 USD/);

    cy.get('div[data-testid="piechart"]').as('pieChart');
    cy.get('@pieChart').find('ul li').first().contains('Project 1');
    cy.get('@pieChart').find('ul li').last().contains('Project 2');
    cy.get('@pieChart').contains('52 %');
    cy.get('@pieChart').contains('48 %');
    cy.get('@pieChart')
      .find('p')
      .contains(/GATEWAY TOTAL: 123318.390 USD/i);
  });

  it('should generate Reports for Project 1 and Gateway 1', () => {
    cy.get('select[name="select-project"]').select('Project 1');
    cy.get('select[name="select-gateway"]').select('Gateway 1');
    cy.get('input[title="From date"]').type('2021-04-21');
    cy.get('input[title="To date"]').type('2021-08-05');
    cy.get('button').contains('Generate report').click();
    cy.get('div[data-testid="table"]').as('table');
    cy.get('@table').contains('h3', 'Project 1 | Gateway 1');

    cy.get('@table').find('ul li:nth-child(1)').contains('Project 1');
    cy.get('@table')
      .find('ul li:nth-child(1)')
      .contains(/TOTAL: 25782.110 USD/);
    cy.get('@table').find('ul li').should('have.length', 1);
  });

  it('should not find Reports for date range bigger than max date of 2021-12-30', () => {
    cy.get('select[name="select-project"]').select('Project 1');
    cy.get('select[name="select-gateway"]').select('Gateway 1');
    cy.get('input[title="From date"]').type('2022-04-21');
    cy.get('input[title="To date"]').type('2022-08-05');
    cy.get('button').contains('Generate report').click();
    cy.get('div[data-testid="table"]').should('not.exist');
    cy.contains(
      'Currently you have no data for the reports to be generated. Once you start generating traffic through the Balance application the reports will be shown.',
    );
    cy.get('img[alt="No Report Image"]');
    cy.get('footer').contains('Terms&Conditions | Privacy policy');
  });
});
