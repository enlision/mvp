import { defineConfig } from 'cypress';
import dotenv from 'dotenv';

dotenv.config();

export default defineConfig({
  component: {
    devServer: {
      framework: 'react',
      bundler: 'vite',
    },
  },

  e2e: {
    video: false,
    setupNodeEvents(on, config) {
      // implement node event listeners here

      // return {
      //   ...config,
      //   browsers: config.browsers.filter((b) => b.family === 'chromium'),
      // };
    },
    baseUrl: process.env.VITE_CLIENT_URL,
    // retries: { runMode: 3, openMode: 3 },
  },
});
