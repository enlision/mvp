import React from 'react';
import { QueryClientProvider } from '@tanstack/react-query';
import { queryConfig } from 'lib';
import Router from 'Router';

import './App.css';

function App() {
  return (
    <div className="App">
      <QueryClientProvider client={queryConfig}>
        <Router />
      </QueryClientProvider>
    </div>
  );
}

export default App;
