import { User, UserResponse } from 'types';

const users: User[] = [
  {
    userId: 'rahej',
    firstName: 'John',
    lastName: 'Doe',
    email: 'john.doe@email.com',
  },
];
export const userResponse: UserResponse = {
  code: '200',
  data: users,
  error: null,
};
