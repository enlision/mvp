const getQueryKey = {
  users: () => ['users'],
  projects: () => ['projects'],
  gateways: () => ['gateways'],
  report: () => ['report'],
};

export default getQueryKey;
