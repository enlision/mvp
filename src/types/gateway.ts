export type Gateway = {
  gatewayId: string;
  userIds: string[];
  name: string;
  type: string;
  apiKey: string;
  secondaryApiKey: string;
  description: string;
};

export type GetGatewaysResponse = {
  code: string;
  data: Gateway[];
  error: null | object;
};
