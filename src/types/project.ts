export type Project = {
  projectId: string;
  userIds: string[];
  rule: string;
  gatewayIds: string[];
  structure: string;
  industry: string;
  website: string;
  description: string;
  image: string;
  name: string;
};

export type GetProjectsResponse = {
  code: string;
  data: Project[];
  error: null | object;
};
