export type Report = {
  paymentId: string;
  amount: number;
  projectId: string;
  gatewayId: string;
  userIds: string[];
  modified: string;
  created: string;
};

export type ReportResponse = {
  code: string;
  data: Report[];
  error: object | null;
};
