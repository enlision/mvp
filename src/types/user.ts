export type User = {
  userId: string;
  firstName: string;
  lastName: string;
  email: string;
};

export type UserResponse = {
  code: '200';
  data: User[];
  error: object | null;
};
