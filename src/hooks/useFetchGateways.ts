import { useQuery } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { getQueryKey } from 'utils';
import { GetGatewaysResponse } from 'types';
import { gatewayResponse } from 'config';

type TData = GetGatewaysResponse;
type TQueryFnData = TData;
type TError = AxiosError;

const useFetchGateways = () => {
  const queryKey = getQueryKey.gateways();

  const queryFn = async () => Promise.resolve(gatewayResponse);
  const response = useQuery<TQueryFnData, TError, TData>(queryKey, queryFn);

  return response;
};

export default useFetchGateways;
