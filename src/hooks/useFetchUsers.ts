import { useQuery } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { getQueryKey } from 'utils';
import { UserResponse } from 'types';
import { userResponse } from 'config';

type TData = UserResponse;
type TQueryFnData = TData;
type TError = AxiosError;

const useFetchUsers = () => {
  const queryKey = getQueryKey.users();

  const queryFn = async () => Promise.resolve(userResponse);
  const response = useQuery<TQueryFnData, TError, TData>(queryKey, queryFn);
  return response;
};

export default useFetchUsers;
