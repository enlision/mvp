import { useQuery } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { getQueryKey } from 'utils';
import { GetProjectsResponse } from 'types';
import { projectResponse } from 'config';

type TData = GetProjectsResponse;
type TQueryFnData = TData;
type TError = AxiosError;

const useFetchProjects = () => {
  const queryKey = getQueryKey.projects();

  const queryFn = async () => Promise.resolve(projectResponse);
  const response = useQuery<TQueryFnData, TError, TData>(queryKey, queryFn);

  return response;
};

export default useFetchProjects;
