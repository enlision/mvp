import { useMutation } from '@tanstack/react-query';

import { getQueryKey } from 'utils';
import { AxiosError } from 'axios';
import { ReportResponse } from 'types';
import { reportsResponse } from 'config';

type TData = ReportResponse;
type TVariables = {
  from?: string;
  to?: string;
  projectId: string;
  gatewayId: string;
};
type TError = AxiosError;
type TContext = unknown;

export const useCreateReport = () => {
  const queryKey = getQueryKey.report();

  const response = useMutation<TData, TError, TVariables, TContext>(
    queryKey,
    ({ from, gatewayId, projectId, to }) => {
      const filteredResults = reportsResponse.data.filter((report) => {
        const fromCondition =
          !from || new Date(from).getTime() < new Date(report.created).getTime();
        const toCondition = !to || new Date(to).getTime() > new Date(report.created).getTime();
        const gatewayIdCondition = !gatewayId || report.gatewayId === gatewayId;
        const projectIdCondition = !projectId || report.projectId === projectId;

        if (fromCondition && toCondition && gatewayIdCondition && projectIdCondition) {
          return true;
        }
        return false;
      });

      const newResponse: ReportResponse = { ...reportsResponse, data: filteredResults };

      return Promise.resolve(newResponse);
    },
  );

  return response;
};

export default useCreateReport;
