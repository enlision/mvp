import { QueryClient } from '@tanstack/react-query';

const queryConfig = new QueryClient({
  defaultOptions: {
    queries: {
      refetchInterval: false,
      refetchOnWindowFocus: false,
    },
  },
});

export default queryConfig;
