import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: process.env.VITE_SERVER_BASE_URL,
  timeout: 30000,
});

// INTERCEPTORS

axiosInstance.interceptors.request.use(
  (config) => {
    // Attach some headers to the config
    // ...
    return config;
  },
  (error) => {
    return Promise.reject(error);
  },
);

axiosInstance.interceptors.response.use(
  (response) => {
    // Do something with response data
    return response.data;
  },
  (error) => {
    // Do something with the error data
    return Promise.reject(error);
  },
);

export default axiosInstance;
