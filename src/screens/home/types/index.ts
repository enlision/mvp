import { Report } from 'types';

export type Option = { label: string; value: string };
export type OptionsKeyValuePair = { [value: string]: Option };

export type ReportsData = {
  reports: Report[];
  projectName: string;
  gatewayName: string;
};

export type ProjectIdOrGatewayIdProperty = Extract<keyof Report, 'gatewayId' | 'projectId'>;
