import { Report } from 'types';
import { groupReportsByProjectIdOrGatewayId } from '.';

describe('groupReportsByProjectIdOrGatewayId', () => {
  it('should group reports by project ID', () => {
    const reports = [
      { amount: 100, projectId: '1', gatewayId: 'A' },
      { amount: 200, projectId: '2', gatewayId: 'B' },
      { amount: 300, projectId: '1', gatewayId: 'C' },
      { amount: 400, projectId: '2', gatewayId: 'D' },
      { amount: 500, projectId: '3', gatewayId: 'E' },
    ] as Report[];

    const result = groupReportsByProjectIdOrGatewayId(reports, 'projectId');

    expect(result).toEqual({
      '1': [
        { amount: 100, projectId: '1', gatewayId: 'A' },
        { amount: 300, projectId: '1', gatewayId: 'C' },
      ],
      '2': [
        { amount: 200, projectId: '2', gatewayId: 'B' },
        { amount: 400, projectId: '2', gatewayId: 'D' },
      ],
      '3': [{ amount: 500, projectId: '3', gatewayId: 'E' }],
    });
  });

  it('should group reports by gateway ID', () => {
    const reports = [
      { amount: 100, projectId: '1', gatewayId: 'A' },
      { amount: 200, projectId: '2', gatewayId: 'B' },
      { amount: 300, projectId: '1', gatewayId: 'C' },
      { amount: 400, projectId: '2', gatewayId: 'B' },
      { amount: 500, projectId: '3', gatewayId: 'E' },
    ] as Report[];

    const result = groupReportsByProjectIdOrGatewayId(reports, 'gatewayId');

    expect(result).toEqual({
      A: [{ amount: 100, projectId: '1', gatewayId: 'A' }],
      B: [
        { amount: 200, projectId: '2', gatewayId: 'B' },
        { amount: 400, projectId: '2', gatewayId: 'B' },
      ],
      C: [{ amount: 300, projectId: '1', gatewayId: 'C' }],
      E: [{ amount: 500, projectId: '3', gatewayId: 'E' }],
    });
  });
});
