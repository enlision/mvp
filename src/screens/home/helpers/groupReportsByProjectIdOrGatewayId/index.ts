import { Report } from 'types';
import { ProjectIdOrGatewayIdProperty } from 'screens/home/types';

export const groupReportsByProjectIdOrGatewayId = (
  reports: Report[],
  projectIdOrGatewayId: ProjectIdOrGatewayIdProperty,
) => {
  return reports.reduce((acc, report) => {
    const id = report[projectIdOrGatewayId];
    if (!acc[id]) {
      acc[id] = [report];
    } else {
      acc[id].push(report);
    }
    return acc;
  }, {} as { [id: string]: Report[] });
};
