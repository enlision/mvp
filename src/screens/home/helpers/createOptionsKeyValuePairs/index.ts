import { OptionsKeyValuePair, Option } from 'screens/home/types';

export const createOptionsKeyValuePairs = (projectOptions: Option[]) =>
  projectOptions
    .filter((option) => option.value)
    .reduce((acc, option) => {
      const id = option.value;
      if (!acc[id]) {
        acc[id] = option;
      }
      return acc;
    }, {} as OptionsKeyValuePair);
