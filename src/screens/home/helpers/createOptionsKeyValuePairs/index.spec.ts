import { createOptionsKeyValuePairs } from '.';

describe('createOptionsKeyValuePairs', () => {
  it('should return an empty object if no options are provided', () => {
    const result = createOptionsKeyValuePairs([]);
    expect(result).toEqual({});
  });

  it('should return an object with options grouped by their values', () => {
    const projectOptions = [
      { value: '1', label: 'Option 1' },
      { value: '2', label: 'Option 2' },
      { value: '3', label: 'Option 3' },
    ];
    const result = createOptionsKeyValuePairs(projectOptions);
    expect(result).toEqual({
      '1': { value: '1', label: 'Option 1' },
      '2': { value: '2', label: 'Option 2' },
      '3': { value: '3', label: 'Option 3' },
    });
  });

  it('should filter out options without a value property', () => {
    const projectOptions = [
      { value: '1', label: 'Option 1' },
      { value: '3', label: 'Option 3' },
    ];
    const result = createOptionsKeyValuePairs(projectOptions);
    expect(result).toEqual({
      '1': { value: '1', label: 'Option 1' },
      '3': { value: '3', label: 'Option 3' },
    });
  });
});
