import { sortObjectByKey } from '.';

describe('sortObjectByKey', () => {
  const optionsKeyValuePair = {
    project1: { label: 'project 1', value: 'project1' },
    project2: { label: 'project 2', value: 'project2' },
    project3: { label: 'project 3', value: 'project3' },
    project4: { label: 'project 4', value: 'project4' },
  };
  const object = {
    project2: [{ amount: 10 }],
    project1: [{ amount: 10 }],
    project3: [{ amount: 10 }],
  };

  it('should sort object by project id', () => {
    const result = sortObjectByKey(object, optionsKeyValuePair);

    expect(result).toEqual({
      project1: [{ amount: 10 }],
      project2: [{ amount: 10 }],
      project3: [{ amount: 10 }],
    });
  });
});
