import { OptionsKeyValuePair } from 'screens/home/types';

type SortObjectByKey = <Obj extends object & { length?: never }>(
  object: Obj,
  optionsKeyValuePair: OptionsKeyValuePair,
) => Obj;

export const sortObjectByKey: SortObjectByKey = (
  object,
  optionsKeyValuePair: OptionsKeyValuePair,
) => {
  return Object.keys(optionsKeyValuePair).reduce((acc, id) => {
    const found = object[id];
    if (found) {
      acc[id] = object[id];
    }
    return acc;
  }, {}) as typeof object;
};
