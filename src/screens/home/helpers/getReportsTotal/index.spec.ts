import { Report } from 'types';
import { getReportsTotal } from '.';

describe('getReportsTotal', () => {
  it('should return 0 if there are no reports', () => {
    expect(getReportsTotal([])).toEqual(0);
  });

  it('should return the sum of all report amounts', () => {
    const reports = [{ amount: 10 }, { amount: 20 }, { amount: 30 }] as Report[];

    expect(getReportsTotal(reports)).toEqual(60);
  });

  it('should handle negative amounts', () => {
    const reports = [{ amount: 10 }, { amount: -20 }, { amount: 30 }] as Report[];

    expect(getReportsTotal(reports)).toEqual(20);
  });
});
