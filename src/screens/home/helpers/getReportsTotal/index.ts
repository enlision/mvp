import { Report } from 'types';

export const getReportsTotal = (reports: Report[]) =>
  reports.reduce((acc, report) => acc + report.amount, 0);
