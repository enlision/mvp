import { Report } from 'types';
import { getReportsTotal } from '../getReportsTotal';
import { groupReportsByProjectIdOrGatewayId } from '../groupReportsByProjectIdOrGatewayId';
import { OptionsKeyValuePair, ProjectIdOrGatewayIdProperty } from 'screens/home/types';
import { sortObjectByKey } from '../sortObjectByKey';

export const groupTotalsForSelectedPropertyIdOrGatewayId = (
  reports: Report[],
  selectedProperty: ProjectIdOrGatewayIdProperty,
  selectedPropertyId: string,
  optionsKeyValuePair: OptionsKeyValuePair,
) => {
  const propertyToGroupReportsBy: ProjectIdOrGatewayIdProperty =
    selectedProperty === 'projectId' ? 'gatewayId' : 'projectId';

  const filteredReports = reports.filter(
    (report) => report[selectedProperty] === selectedPropertyId,
  );
  const groupedReportsById = groupReportsByProjectIdOrGatewayId(
    filteredReports,
    propertyToGroupReportsBy,
  );
  const sortedPercentageOfTotalById = sortObjectByKey(groupedReportsById, optionsKeyValuePair);

  const total = getReportsTotal(filteredReports);
  const percentageOfTotalById = Object.keys(sortedPercentageOfTotalById).reduce((acc, id) => {
    const reportsTotalById = getReportsTotal(groupedReportsById[id]);
    const percentage = (reportsTotalById / total) * 100;
    acc[id] = percentage;
    return acc;
  }, {} as { [id: string]: number });

  return {
    total,
    percentageOfTotalById,
  };
};
