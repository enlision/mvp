import { Report } from 'types';
import { groupTotalsForSelectedPropertyIdOrGatewayId } from '.';
import { OptionsKeyValuePair } from 'screens/home/types';

describe('getReportsTotal', () => {
  const projectOptionsKeyValuePair: OptionsKeyValuePair = {
    ERdPQ: { label: 'Project 1', value: 'ERdPQ' },
    bgYhx: { label: 'Project 2', value: 'bgYhx' },
  };

  const gatewayOptionsKeyValuePair: OptionsKeyValuePair = {
    i6ssp: { label: 'Gateway 1', value: 'i6ssp' },
    GzFF8: { label: 'Gateway 2', value: 'GzFF8' },
  };
  const reports: Report[] = [
    {
      paymentId: '6149cf56b33efe4c5a481c51',
      amount: 100,
      projectId: 'ERdPQ',
      gatewayId: 'i6ssp',
      userIds: ['rahej'],
      modified: '2021-06-23',
      created: '2021-09-18',
    },
    {
      paymentId: '6149cf564334774f024da96e',
      amount: 100,
      projectId: 'bgYhx',
      gatewayId: 'i6ssp',
      userIds: ['rahej'],
      modified: '2021-03-07',
      created: '2021-09-16',
    },
    {
      paymentId: '6149cf56d7ba96e40620e0cd',
      amount: 100,
      projectId: 'bgYhx',
      gatewayId: 'GzFF8',
      userIds: ['rahej'],
      modified: '2021-03-21',
      created: '2021-09-12',
    },
    {
      paymentId: '6149cf56adf722884f0d339f',
      amount: 100,
      projectId: 'bgYhx',
      gatewayId: 'i6ssp',
      userIds: ['rahej'],
      modified: '2021-08-21',
      created: '2021-09-20',
    },
  ];

  it('should return the sum of all report amounts for selected projectId', () => {
    const projectId = 'ERdPQ';
    const equal: ReturnType<typeof groupTotalsForSelectedPropertyIdOrGatewayId> = {
      total: 100,
      percentageOfTotalById: {
        i6ssp: 100,
      },
    };
    expect(
      groupTotalsForSelectedPropertyIdOrGatewayId(
        reports,
        'projectId',
        projectId,
        gatewayOptionsKeyValuePair,
      ),
    ).toEqual(equal);
  });

  it('should return the sum of all report amounts for selected gatewayId', () => {
    expect(
      groupTotalsForSelectedPropertyIdOrGatewayId(
        reports,
        'gatewayId',
        'i6ssp',
        projectOptionsKeyValuePair,
      ),
    ).toEqual({
      total: 300,
      percentageOfTotalById: {
        ERdPQ: 33.33333333333333,
        bgYhx: 66.66666666666666,
      },
    });

    expect(
      groupTotalsForSelectedPropertyIdOrGatewayId(
        reports,
        'gatewayId',
        'GzFF8',
        projectOptionsKeyValuePair,
      ),
    ).toEqual({
      total: 100,
      percentageOfTotalById: {
        bgYhx: 100,
      },
    });
  });
});
