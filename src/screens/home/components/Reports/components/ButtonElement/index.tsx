import React from 'react';

type Props = {
  text: string;
} & React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>;

const ButtonElement: React.FC<Props> = ({ text, className = '', ...rest }) => {
  return (
    <button
      {...rest}
      className={`
      outline-0 text-white font-normal text-sm leading-4 flex
      items-center justify-between max-h-8 transition-all duration-[0.3s]
      ease-[ease-in-out] px-[13px] py-2 rounded-[5px] border-[none] outline-[none]
      focus:outline-0 focus:outline-transparent focus:border-[none] 
      active:outline-0 active:outline-transparent active:border-[none] 
      hover:bg-[#007ac9] active:bg-[##0064a5] bg-[#005b96] ${className}`}
    >
      {text}
    </button>
  );
};

export default ButtonElement;
