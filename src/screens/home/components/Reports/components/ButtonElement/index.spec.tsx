import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import ButtonElement from '.';

describe('ButtonElement', () => {
  it('should render correctly and call onClick handler', () => {
    const onClick = jest.fn((value) => value);
    render(<ButtonElement text="My button" onClick={onClick} />);
    const buttonElement = screen.getByText('My button');
    expect(buttonElement).toBeInTheDocument();

    fireEvent.click(buttonElement);
    expect(onClick).toHaveBeenCalledTimes(1);
  });
});
