import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import SelectElement from '.';

describe('SelectElement', () => {
  it('should render correctly and call onChange handler with passed value', () => {
    const onChange = jest.fn((value) => value);
    render(
      <SelectElement
        onChange={onChange}
        options={[
          { label: 'Option 1', value: 'value 1' },
          { label: 'Option 2', value: 'value 2' },
        ]}
        value=""
        dataTestId="select-element"
      />,
    );
    const selectElement = screen.getByTestId('select-element');
    expect(selectElement).toBeInTheDocument();
    const option = screen.getByRole('option', { name: 'Option 2' });
    userEvent.selectOptions(selectElement, option);
    expect(onChange.mock.lastCall?.[0]).toEqual('value 2');
    expect(onChange).toHaveBeenCalledTimes(1);
  });
});
