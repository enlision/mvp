import React from 'react';

type Props = {
  value: string | undefined;
  onChange: (value: string) => void;
  className?: string;
  options: { value: string; label: string }[];
  dataTestId?: string;
};

const SelectElement: React.FC<Props> = ({ dataTestId, options, className, value, onChange }) => {
  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const newValue = e.target.value;
    onChange(newValue);
  };
  return (
    <select
      name={dataTestId}
      data-testid={dataTestId}
      value={value}
      onChange={handleChange}
      className={`cursor-pointer bg-[#1bc5bd] active:bg-[#1dd2ca] hover:bg-[#30e3da] outline-0 text-white font-normal text-sm leading-4 max-h-8 transition-all duration-[0.3s] ease-[ease-in-out] px-[13px] py-2 rounded-[5px] border-[none] ${className}`}
    >
      {options.map(({ value, label }) => (
        <option key={value} value={value}>
          {label}
        </option>
      ))}
    </select>
  );
};

export default SelectElement;
