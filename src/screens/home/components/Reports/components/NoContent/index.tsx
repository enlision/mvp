import React from 'react';

import NoReportImage from 'screens/home/assets/no-reports.png';

const NoContent = () => {
  return (
    <div className="text-center max-w-[470px] mt-[138px] mb-0 mx-auto">
      <h3 className="not-italic font-bold text-2xl leading-7 text-center text-[#011f4b] mb-1">
        No reports
      </h3>
      <p className="not-italic font-bold text-base leading-[19px] text-center text-[#7e8299]">
        Currently you have no data for the reports to be generated. Once you start generating
        traffic through the Balance application the reports will be shown.
      </p>
      <img className="w-[402.5px] h-[171px] mt-[51px]" src={NoReportImage} alt="No Report Image" />
    </div>
  );
};

export default NoContent;
