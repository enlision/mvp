import React from 'react';
import { render, screen } from '@testing-library/react';
import NoContent from '.';

describe('NoContent', () => {
  it('should render correctly', () => {
    render(<NoContent />);
    expect(screen.getByText(/No reports/i)).toBeInTheDocument();
    expect(
      screen.getByText(/Currently you have no data for the reports to be generated/i),
    ).toBeInTheDocument();
    expect(screen.getByAltText(/No Report Image/i)).toBeInTheDocument();
  });
});
