import React from 'react';
import { render, screen } from '@testing-library/react';
import Table from '.';

const reportsData = {
  reports: [
    {
      paymentId: '6149cf567833e57669e60455',
      amount: 2663.69,
      projectId: 'bgYhx',
      gatewayId: 'i6ssp',
      userIds: ['rahej'],
      modified: '2021-09-20',
      created: '2021-04-11',
    },
  ],
  gatewayName: 'All gateways',
  projectName: 'Project 1',
};

const projectOptionsKeyValuePairs = {
  ERdPQ: { value: 'ERdPQ', label: 'Project 2' },
  bgYhx: { value: 'bgYhx', label: 'Project 1' },
};

const gatewayOptionsKeyValuePairs = {
  GzFF8: { value: '"GzFF8"', label: 'Gateway 2' },
  i6ssp: { value: 'i6ssp', label: 'Gateway 1' },
};

describe('Table', () => {
  it('should render table headers and report data', () => {
    render(
      <Table
        reportsData={reportsData}
        projectOptionsKeyValuePairs={projectOptionsKeyValuePairs}
        gatewayOptionsKeyValuePairs={gatewayOptionsKeyValuePairs}
      />,
    );
    const report = reportsData.reports[0];
    const reportGatewayName = gatewayOptionsKeyValuePairs[report.gatewayId].label;

    // Check if project name and gateway name are rendered
    expect(
      screen.getByText(`${reportsData.projectName} | ${reportsData.gatewayName}`),
    ).toBeInTheDocument();

    // Check if project item is rendered
    expect(screen.getByText(reportsData.projectName)).toBeInTheDocument();

    expect(screen.getByText(`TOTAL: ${report.amount.toFixed(3)} USD`)).toBeInTheDocument();
    // Check if report data is rendered
    expect(screen.getByText(report.created)).toBeInTheDocument();
    expect(screen.getByText(reportGatewayName)).toBeInTheDocument();
    expect(screen.getByText(report.paymentId)).toBeInTheDocument();
    expect(screen.getByText(`${report.amount} USD`)).toBeInTheDocument();
  });
});
