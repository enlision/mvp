import React from 'react';
import { sortObjectByKey } from 'screens/home/helpers';
import { groupReportsByProjectIdOrGatewayId } from 'screens/home/helpers/groupReportsByProjectIdOrGatewayId';

import { OptionsKeyValuePair, ProjectIdOrGatewayIdProperty, ReportsData } from 'screens/home/types';
import { Report } from 'types';

type Props = {
  reportsData: ReportsData;
  projectOptionsKeyValuePairs: OptionsKeyValuePair;
  gatewayOptionsKeyValuePairs: OptionsKeyValuePair;
};

const Table: React.FC<Props> = ({
  reportsData: { reports, gatewayName, projectName },
  projectOptionsKeyValuePairs,
  gatewayOptionsKeyValuePairs,
}) => {
  const parseReportsDataByProjectIdOrGatewayId = (
    reports: Report[],
    projectOptionsValueOptionPair: OptionsKeyValuePair,
    projectIdOrGatewayId: ProjectIdOrGatewayIdProperty,
  ) => {
    const groupedReportsByProjectIdOrGatewayId = groupReportsByProjectIdOrGatewayId(
      reports,
      projectIdOrGatewayId,
    );

    const sortedReportsByProjectName = sortObjectByKey(
      groupedReportsByProjectIdOrGatewayId,
      projectOptionsValueOptionPair,
    );

    return sortedReportsByProjectName;
  };

  const reportsGroupedByProjectId = parseReportsDataByProjectIdOrGatewayId(
    reports,
    projectOptionsKeyValuePairs,
    'projectId',
  );

  return (
    <div data-testid="table" className="px-6 py-4 rounded-[10px] bg-[#f1fafe]">
      <h3 className="font-bold text-base leading-[19px] text-[#011f4b] mt-0 mb-8 mx-0;">
        {projectName} | {gatewayName}
      </h3>
      <ul className="mt-0 pl-0">
        {Object.keys(reportsGroupedByProjectId).map((projectId, index) => {
          const projectReports = reportsGroupedByProjectId[projectId];
          const projectName = projectOptionsKeyValuePairs[projectId]?.label;
          const showGatewayColumn = !Object.values(gatewayOptionsKeyValuePairs).find(
            (option) => option.label === gatewayName,
          );

          const totalProjectAmount = projectReports.reduce((acc, report) => acc + report.amount, 0);

          return (
            <li key={index} className="mt-4">
              <div className="flex items-center justify-between p-[25px] rounded-[10px] bg-white">
                <div className="font-bold text-base leading-[19px] text-[#011f4b]">
                  {projectName}
                </div>
                <div className="font-bold text-base leading-[26px] text-[#011f4b] uppercase">
                  TOTAL: {totalProjectAmount.toFixed(3)} USD
                </div>
              </div>
              <table className="w-full mt-4">
                <thead>
                  <tr>
                    <th className="px-6 py-2.5">Date</th>
                    {showGatewayColumn && <th className="px-6 py-2.5 text-center">Gateway</th>}
                    <th className="px-6 py-2.5 text-center">Transaction ID</th>
                    <th className="px-6 py-2.5 text-right">Amount</th>
                  </tr>
                </thead>

                <tbody>
                  {projectReports.map((report) => {
                    const { created, gatewayId, paymentId, amount } = report;
                    const reportGatewayName = gatewayOptionsKeyValuePairs[gatewayId].label;
                    return (
                      <tr key={report.paymentId}>
                        <td className="px-6 py-2.5">{created}</td>
                        {showGatewayColumn && (
                          <td className="px-6 py-2.5 text-center">{reportGatewayName}</td>
                        )}
                        <td className="text-center">{paymentId}</td>
                        <td className="px-6 py-2.5 text-right">{amount} USD</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default Table;
