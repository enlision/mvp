import React from 'react';

import styles from './styles.module.scss';

type Props = {
  value: string | undefined;
  onChange: (value: string) => void;
  dataTestId?: string;
} & Omit<
  React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>,
  'type' | 'hidden' | 'value' | 'onChange'
>;

const DateElement: React.FC<Props> = ({
  className = '',
  dataTestId,
  title,
  value,
  onChange,
  ...rest
}) => {
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newValue = e.target.value;
    onChange(newValue);
  };

  return (
    <div
      className={`bg-[#1bc5bd] hover:bg-[#30e3da] active:bg-[#1dd2ca] outline-0 text-white font-normal text-sm max-h-8 leading-8 transition-all duration-[0.3s] ease-[ease-in-out] relative px-[13px] py-0 rounded-[5px] border-[none] ${className}`}
    >
      {value || title}
      <input
        data-testid={dataTestId}
        className={styles.date}
        value={value}
        onChange={handleChange}
        type="date"
        {...rest}
        title={title}
      />
    </div>
  );
};

export default DateElement;
