import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import DateElement from '.';

describe('DateElement', () => {
  it('should render correctly and call onChange handler with passed value', () => {
    const onChange = jest.fn((value) => value);
    render(
      <DateElement
        name="date-input"
        dataTestId="date-input"
        onChange={onChange}
        title="Date"
        value="2021-01-01"
        min="2021-01-01"
        max="2023-01-01"
      />,
    );
    const dateElement = screen.getByTestId('date-input');
    expect(dateElement).toBeInTheDocument();

    fireEvent.change(dateElement, { target: { value: '2022-01-01' } });
    expect(onChange.mock.lastCall?.[0]).toEqual('2022-01-01');
    expect(onChange).toHaveBeenCalledTimes(1);
  });
});
