import React from 'react';
import { render, screen } from '@testing-library/react';
import PieChart from '.';

describe('PieChart', () => {
  it('should not render as gatewayName and projectName are selected', () => {
    render(
      <PieChart
        gatewayOptionsKeyValuePairs={{
          gateway1_id: { label: 'Gateway 1', value: 'gateway1_id' },
          gateway2_id: { label: 'Gateway 2', value: 'gateway2_id' },
        }}
        projectOptionsKeyValuePairs={{
          project1_id: { label: 'Project 1', value: 'project1_id' },
          // project2_id: { label: 'Project 2', value: 'project2_id' },
        }}
        reportsData={{
          gatewayName: 'Gateway 1',
          projectName: 'Project 1',
          reports: [],
        }}
      />,
    );
    expect(screen.queryByText(/Project Total:/i)).toBeNull();
    expect(screen.queryByText(/Gateway Total:/i)).toBeNull();
  });
  it('should render Project 1 and belonging gateways pie chart info', () => {
    render(
      <PieChart
        gatewayOptionsKeyValuePairs={{
          gateway1_id: { label: 'Gateway 1', value: 'gateway1_id' },
          gateway2_id: { label: 'Gateway 2', value: 'gateway2_id' },
        }}
        projectOptionsKeyValuePairs={{
          project1_id: { label: 'Project 1', value: 'project1_id' },
          // project2_id: { label: 'Project 2', value: 'project2_id' },
        }}
        reportsData={{
          gatewayName: '',
          projectName: 'Project 1',
          reports: [
            {
              paymentId: 'paymentId',
              amount: 5,
              projectId: 'project1_id',
              gatewayId: 'gateway1_id',
              userIds: ['1', '2', '3'],
              modified: 'modified',
              created: 'created',
            },
            {
              paymentId: 'paymentId',
              amount: 10,
              projectId: 'project1_id',
              gatewayId: 'gateway1_id',
              userIds: ['1', '2', '3'],
              modified: 'modified',
              created: 'created',
            },
            {
              paymentId: 'paymentId',
              amount: 25,
              projectId: 'project1_id',
              gatewayId: 'gateway2_id',
              userIds: ['1', '2', '3'],
              modified: 'modified',
              created: 'created',
            },
            {
              paymentId: 'paymentId',
              amount: 60,
              projectId: 'project1_id',
              gatewayId: 'gateway2_id',
              userIds: ['1', '2', '3'],
              modified: 'modified',
              created: 'created',
            },
            {
              paymentId: 'paymentId',
              amount: 100,
              projectId: 'project2_id',
              gatewayId: 'gateway2_id',
              userIds: ['1', '2', '3'],
              modified: 'modified',
              created: 'created',
            },
          ],
        }}
      />,
    );
    expect(screen.getByText(/Gateway 1/i)).toBeInTheDocument();
    expect(screen.getByText(/15 %/i)).toBeInTheDocument();
    expect(screen.getByText(/Gateway 2/i)).toBeInTheDocument();
    expect(screen.getByText(/85 %/i)).toBeInTheDocument();
    expect(screen.getByText(/Project Total: 100.000 USD/i)).toBeInTheDocument();
  });
  it('should render Gateway 1 and belonging projects pie chart info', () => {
    render(
      <PieChart
        gatewayOptionsKeyValuePairs={{
          gateway1_id: { label: 'Gateway 1', value: 'gateway1_id' },
          gateway2_id: { label: 'Gateway 2', value: 'gateway2_id' },
        }}
        projectOptionsKeyValuePairs={{
          project1_id: { label: 'Project 1', value: 'project1_id' },
          project2_id: { label: 'Project 2', value: 'project2_id' },
        }}
        reportsData={{
          gatewayName: 'Gateway 1',
          projectName: '',
          reports: [
            {
              paymentId: 'paymentId',
              amount: 20,
              projectId: 'project1_id',
              gatewayId: 'gateway1_id',
              userIds: ['1', '2', '3'],
              modified: 'modified',
              created: 'created',
            },
            {
              paymentId: 'paymentId',
              amount: 20,
              projectId: 'project1_id',
              gatewayId: 'gateway1_id',
              userIds: ['1', '2', '3'],
              modified: 'modified',
              created: 'created',
            },
            {
              paymentId: 'paymentId',
              amount: 20,
              projectId: 'project1_id',
              gatewayId: 'gateway1_id',
              userIds: ['1', '2', '3'],
              modified: 'modified',
              created: 'created',
            },
            {
              paymentId: 'paymentId',
              amount: 20,
              projectId: 'project2_id',
              gatewayId: 'gateway1_id',
              userIds: ['1', '2', '3'],
              modified: 'modified',
              created: 'created',
            },
            {
              paymentId: 'paymentId',
              amount: 20,
              projectId: 'project2_id',
              gatewayId: 'gateway1_id',
              userIds: ['1', '2', '3'],
              modified: 'modified',
              created: 'created',
            },
          ],
        }}
      />,
    );
    expect(screen.getByText(/Project 1/i)).toBeInTheDocument();
    expect(screen.getByText(/60 %/i)).toBeInTheDocument();
    expect(screen.getByText(/Project 2/i)).toBeInTheDocument();
    expect(screen.getByText(/40 %/i)).toBeInTheDocument();
    expect(screen.getByText(/Gateway Total: 100.000 USD/i)).toBeInTheDocument();
  });
});
