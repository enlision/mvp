import React from 'react';
import { VictoryPie } from 'victory';
import { ReportsData, OptionsKeyValuePair } from 'screens/home/types';

import { groupTotalsForSelectedPropertyIdOrGatewayId } from 'screens/home/helpers';

type Props = {
  reportsData: ReportsData;
  projectOptionsKeyValuePairs: OptionsKeyValuePair;
  gatewayOptionsKeyValuePairs: OptionsKeyValuePair;
};

const COLORS = ['#A259FF', '#F24E1E', '#FFC107', '#6497B1'];

const PieChart: React.FC<Props> = ({
  reportsData: { gatewayName, projectName, reports },
  projectOptionsKeyValuePairs,
  gatewayOptionsKeyValuePairs,
}) => {
  const getProjectOrGatewayOption = (
    projectIdOrGatewayName: string,
    groupedOptionsById: OptionsKeyValuePair,
  ) => {
    const option = Object.values(groupedOptionsById).find(
      (opt) => opt.label === projectIdOrGatewayName,
    );
    return option;
  };

  const projectOption = getProjectOrGatewayOption(projectName, projectOptionsKeyValuePairs);
  const gatewayOption = getProjectOrGatewayOption(gatewayName, gatewayOptionsKeyValuePairs);

  const isGatewaySelected = !!gatewayOption?.value;
  const isProjectSelected = !!projectOption?.value;

  const getPieChartData = () => {
    if (isGatewaySelected) {
      const { total, percentageOfTotalById } = groupTotalsForSelectedPropertyIdOrGatewayId(
        reports,
        'gatewayId',
        gatewayOption?.value,
        projectOptionsKeyValuePairs,
      );

      const data = Object.keys(percentageOfTotalById).map((projectId) => ({
        x: projectOptionsKeyValuePairs[projectId].label,
        y: percentageOfTotalById?.[projectId].toFixed(0),
      }));

      return { data, total };
    }
    if (isProjectSelected) {
      const { total, percentageOfTotalById } = groupTotalsForSelectedPropertyIdOrGatewayId(
        reports,
        'projectId',
        projectOption?.value,
        gatewayOptionsKeyValuePairs,
      );

      const data = Object.keys(percentageOfTotalById).map((gatewayId) => ({
        x: gatewayOptionsKeyValuePairs[gatewayId].label,
        y: percentageOfTotalById?.[gatewayId].toFixed(0),
      }));

      return { data, total };
    }

    return { data: undefined, total: undefined };
  };

  const { data, total } = getPieChartData();

  if (isGatewaySelected && isProjectSelected) return <div />;

  return (
    <div data-testid="piechart" className="grid grid-rows-[auto_1fr_auto] h-full">
      {data && (
        <>
          <ul className="flex m-0 px-6 py-[18px] rounded-[10px] bg-[#f1fafe] list-none">
            {data.map((point, index) => (
              <li className="flex items-center mr-9" key={point.x}>
                <span
                  className="w-[15px] h-[15px] mr-3 rounded-[5px]"
                  style={{ backgroundColor: COLORS[index] }}
                ></span>
                <span className="font-normal text-sm leading-4 text-[#011f4b]">{point.x}</span>
              </li>
            ))}
          </ul>
          <div className="flex items-center justify-center">
            <svg width={700} height={700}>
              <circle cx={150} cy={150} r={50} fill="#fff" />
              <VictoryPie
                innerRadius={180}
                standalone={false}
                height={700}
                width={700}
                data={data}
                colorScale={COLORS}
                labels={({ datum }) => `${datum.y} %`}
              />
            </svg>
          </div>
          <p className="uppercase font-bold text-base leading-[19px] text-[#011f4b] mt-[30px] mb-0 px-6 py-[18px] rounded-[10px] bg-[#f1fafe]">
            {isProjectSelected ? `Project Total:` : 'Gateway Total:'} {total?.toFixed(3)} USD
          </p>
        </>
      )}
    </div>
  );
};

export default PieChart;
