import React from 'react';
import { render, screen, fireEvent, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Reports from '.';

import { useCreateReport, useFetchGateways, useFetchProjects, useFetchUsers } from 'hooks';

jest.mock('hooks/useFetchGateways.ts');
jest.mock('hooks/useFetchProjects.ts');
jest.mock('hooks/useCreateReport.ts');
jest.mock('hooks/useFetchUsers.ts');
jest.mock('./components', () => ({
  ...jest.requireActual('./components'),
  Table: () => <div>Table</div>,
  PieChart: () => <div>PieChart</div>,
  NoContent: () => <div>NoContent</div>,
}));

(useCreateReport as jest.Mock<Partial<ReturnType<typeof useCreateReport>>>).mockReturnValue({
  isLoading: false,
  mutateAsync: async () => ({
    data: [
      {
        paymentId: 'paymentId',
        amount: 10,
        projectId: 'projectId',
        gatewayId: 'gatewayId',
        userIds: ['1', '2', '3'],
        modified: 'modified',
        created: 'created',
      },
    ],
    code: '200',
    error: null,
  }),
});

(useFetchProjects as jest.Mock<Partial<ReturnType<typeof useFetchProjects>>>).mockReturnValue({
  isLoading: false,
  data: {
    data: [
      {
        description: 'description',
        gatewayIds: ['1', '2', '3'],
        image: 'image',
        industry: 'industry',
        name: 'Project 1',
        rule: 'rule',
        structure: 'structure',
        website: 'website',
        projectId: 'projectId',
        userIds: ['1', '2', '3'],
      },
    ],
    code: '200',
    error: null,
  },
});

(useFetchGateways as jest.Mock<Partial<ReturnType<typeof useFetchGateways>>>).mockReturnValue({
  isLoading: false,
  data: {
    data: [
      {
        description: 'description',
        apiKey: 'apiKey',
        gatewayId: 'gatewayId',
        secondaryApiKey: 'secondaryApiKey',
        type: 'type',
        name: 'Gateway 1',
        userIds: ['1', '2', '3'],
      },
    ],
    code: '200',
    error: null,
  },
});

(useFetchUsers as jest.Mock<Partial<ReturnType<typeof useFetchUsers>>>).mockReturnValue({
  isLoading: false,
  data: {
    data: [
      {
        email: 'email',
        firstName: 'firstName',
        lastName: 'lastName',
        userId: 'userId',
      },
    ],
    code: '200',
    error: null,
  },
});
describe('Reports', () => {
  it('should render Reports with NoContent initially', () => {
    render(<Reports />);
    expect(screen.getByText(/All projects/i)).toBeInTheDocument();
    expect(screen.getByText(/All gateways/i)).toBeInTheDocument();
    expect(screen.getByText(/From date/i)).toBeInTheDocument();
    expect(screen.getByText(/To date/i)).toBeInTheDocument();
    expect(screen.getByText(/Generate report/i)).toBeInTheDocument();
    expect(screen.getByText(/NoContent/i)).toBeInTheDocument();
  });

  it('should render Reports Table for selected all projects and all gateways', async () => {
    render(<Reports />);
    expect(screen.getByText(/All projects/i)).toBeInTheDocument();
    expect(screen.getByText(/All gateways/i)).toBeInTheDocument();
    expect(screen.queryByText(/NoContent/i)).toBeInTheDocument();
    await act(async () => {
      fireEvent.click(screen.getByText(/Generate report/i));
    });
    expect(screen.getByText(/Table/i)).toBeInTheDocument();
    expect(screen.queryByText(/PieChart/i)).toBeNull();
    expect(screen.queryByText(/NoContent/i)).toBeNull();
  });

  it('should render Reports Table and PieChart for App projects and Gateway 1', async () => {
    render(<Reports />);
    expect(screen.getByText(/All projects/i)).toBeInTheDocument();
    const selectElement = screen.getByTestId('select-gateway');
    const option = screen.getByRole('option', { name: 'Gateway 1' });
    expect((option as HTMLOptionElement).selected).toBe(false);
    userEvent.selectOptions(selectElement, option);
    expect((option as HTMLOptionElement).selected).toBe(true);
    await act(async () => {
      fireEvent.click(screen.getByText(/Generate report/i));
    });
    expect(screen.getByText(/Table/i)).toBeInTheDocument();
    expect(screen.getByText(/PieChart/i)).toBeInTheDocument();
    expect(screen.queryByText(/NoContent/i)).toBeNull();
  });

  it('should render only Reports Table for Project 1 and Gateway 1', async () => {
    render(<Reports />);
    expect(screen.getByText(/All projects/i)).toBeInTheDocument();
    const projectsSelect = screen.getByTestId('select-project');
    const projecOption = screen.getByRole('option', { name: 'Project 1' });
    expect((projecOption as HTMLOptionElement).selected).toBe(false);
    userEvent.selectOptions(projectsSelect, projecOption);
    expect((projecOption as HTMLOptionElement).selected).toBe(true);

    const gatewaySelect = screen.getByTestId('select-gateway');
    const gatewayOption = screen.getByRole('option', { name: 'Gateway 1' });
    expect((gatewayOption as HTMLOptionElement).selected).toBe(false);
    userEvent.selectOptions(gatewaySelect, gatewayOption);
    expect((gatewayOption as HTMLOptionElement).selected).toBe(true);

    await act(async () => {
      fireEvent.click(screen.getByText(/Generate report/i));
    });
    expect(screen.getByText(/Table/i)).toBeInTheDocument();
    expect(screen.queryByText(/PieChart/i)).toBeNull();
    expect(screen.queryByText(/NoContent/i)).toBeNull();
  });
});
