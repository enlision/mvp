import { useCreateReport, useFetchGateways, useFetchProjects, useFetchUsers } from 'hooks';
import React, { useState } from 'react';
import { DEFAULT_GATEWAY_OPTION, DEFAULT_PROJECT_OPTION } from 'screens/home/config';
import { createOptionsKeyValuePairs } from 'screens/home/helpers';
import { Option, ReportsData } from 'screens/home/types';
import { Gateway, Project } from 'types';
import {
  ButtonElement,
  DateElement,
  NoContent,
  Table,
  SelectElement,
  PieChart,
} from './components';

const Reports = () => {
  const usersQuery = useFetchUsers();
  const projectsQuery = useFetchProjects();
  const gatewaysQuery = useFetchGateways();
  const reportMutation = useCreateReport();

  const [selectedProjectId, setSelectedProjectId] = useState<string>(DEFAULT_PROJECT_OPTION.value);
  const [selectedGatewayId, setSelectedGatewayId] = useState<string>(DEFAULT_GATEWAY_OPTION.value);
  const [reportsData, setReportsData] = useState<ReportsData>({
    reports: [],
    projectName: DEFAULT_PROJECT_OPTION.label,
    gatewayName: DEFAULT_GATEWAY_OPTION.label,
  });
  const [fromDate, setFromDate] = useState<string | undefined>();
  const [toDate, setToDate] = useState<string | undefined>();

  type GetResourceOptions = <Data extends object[]>(args: {
    defaultOption: Option;
    data: Data | undefined;
    mapOptions: (data: Data) => Option[];
  }) => Option[];

  const getResourceOptions: GetResourceOptions = ({ mapOptions, defaultOption, data }) => {
    if (!data?.length) {
      return [defaultOption];
    }
    const mappedOptions: Option[] = mapOptions(data);
    return [defaultOption, ...mappedOptions];
  };
  if (projectsQuery.isLoading || usersQuery.isLoading || gatewaysQuery.isLoading)
    return <div></div>;

  const mapProjectOptions = (data: Project[]) =>
    data.map((project) => ({
      value: project?.projectId,
      label: project?.name,
    }));

  const mapGatewayOptions = (data: Gateway[]) =>
    data.map((gateway) => ({
      value: gateway?.gatewayId,
      label: gateway?.name,
    }));

  const projectOptions = getResourceOptions<Project[]>({
    mapOptions: mapProjectOptions,
    data: projectsQuery?.data?.data,
    defaultOption: DEFAULT_PROJECT_OPTION,
  });

  const gatewayOptions = getResourceOptions<Gateway[]>({
    mapOptions: mapGatewayOptions,
    data: gatewaysQuery?.data?.data,
    defaultOption: DEFAULT_GATEWAY_OPTION,
  });

  const minDateValue = '2021-01-01';
  const fromDateMaxValue = '2021-12-30';
  const maxDateValue = '2021-12-31';

  const showPieChart =
    reportsData.gatewayName !== DEFAULT_GATEWAY_OPTION.label &&
    reportsData.projectName !== DEFAULT_PROJECT_OPTION.label
      ? false
      : reportsData.gatewayName !== DEFAULT_GATEWAY_OPTION.label ||
        reportsData.projectName !== DEFAULT_PROJECT_OPTION.label;

  const projectOptionsKeyValuePairs = createOptionsKeyValuePairs(projectOptions);
  const gatewayOptionsKeyValuePairs = createOptionsKeyValuePairs(gatewayOptions);
  return (
    <div>
      <div className="flex flex-wrap justify-between gap-7 mb-7">
        <div>
          <h1 data-testid="title" className="font-bold text-2xl leading-7 text-[#011f4b] m-0 mb-1">
            Reports
          </h1>
          <h2
            data-testid="subtitle"
            className="font-bold text-base leading-[19px] text-[#7e8299] m-0"
          >
            Easily generate a report of your transactions
          </h2>
        </div>

        <div className="flex">
          <SelectElement
            dataTestId="select-project"
            value={selectedProjectId}
            onChange={setSelectedProjectId}
            options={projectOptions}
            className="mb-6 mr-6"
          />
          <SelectElement
            dataTestId="select-gateway"
            value={selectedGatewayId}
            onChange={setSelectedGatewayId}
            options={gatewayOptions}
            className="mb-6 mr-6"
          />
          <DateElement
            value={fromDate}
            onChange={setFromDate}
            className="mb-6 mr-6"
            title="From date"
            defaultValue={minDateValue}
            min={minDateValue}
            max={toDate || fromDateMaxValue}
          />
          <DateElement
            value={toDate}
            onChange={setToDate}
            className="mr-6"
            title="To date"
            defaultValue={maxDateValue}
            min={fromDate || minDateValue}
            max={maxDateValue}
          />
          <ButtonElement
            text="Generate report"
            onClick={async () => {
              const reportResponse = await reportMutation.mutateAsync({
                from: fromDate || minDateValue,
                to: toDate || maxDateValue,
                gatewayId: selectedGatewayId,
                projectId: selectedProjectId,
              });

              const projectName =
                projectOptions.find((opt) => opt.value === selectedProjectId)?.label ||
                DEFAULT_PROJECT_OPTION.label ||
                DEFAULT_PROJECT_OPTION.label;
              const gatewayName =
                gatewayOptions.find((opt) => opt.value === selectedGatewayId)?.label ||
                DEFAULT_GATEWAY_OPTION.label;

              setReportsData((prev) => ({
                ...prev,
                reports: reportResponse?.data,
                projectName,
                gatewayName,
              }));
            }}
          />
        </div>
      </div>
      {!reportsData?.reports.length ? (
        <NoContent />
      ) : (
        <div className="flex flex-wrap gap-7">
          <div className="flex-[1]">
            <Table
              projectOptionsKeyValuePairs={projectOptionsKeyValuePairs}
              gatewayOptionsKeyValuePairs={gatewayOptionsKeyValuePairs}
              reportsData={reportsData}
            />
          </div>
          {showPieChart && (
            <div className="flex-[1]">
              <PieChart
                projectOptionsKeyValuePairs={projectOptionsKeyValuePairs}
                gatewayOptionsKeyValuePairs={gatewayOptionsKeyValuePairs}
                reportsData={reportsData}
              />
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default Reports;
