import React, { useState } from 'react';

import { BaseLayout } from 'components';

import FirstIcon from 'screens/home/assets/first-icon.png';
import SecondIcon from 'screens/home/assets/second-icon.png';
import ThirdIcon from 'screens/home/assets/third-icon.png';
import FourthIcon from 'screens/home/assets/fourth-icon.png';
import FifthIcon from 'screens/home/assets/fifth-icon.png';
import { Reports } from './components';

type Pages = 'firstPageMock' | 'secondPageMock' | 'thirdPageMock' | 'reports' | 'fifthPageMock';
type PageData = { src: string; content: () => JSX.Element; order: number; alt: string };
type PageOptions = { [key in Pages]: PageData };

const sideMenuPageOptions: PageOptions = {
  firstPageMock: {
    src: FirstIcon,
    content: () => <div>First page</div>,
    order: 1,
    alt: 'alt',
  },
  secondPageMock: {
    src: SecondIcon,
    content: () => <div>Second page</div>,
    order: 2,
    alt: 'alt',
  },
  thirdPageMock: {
    src: ThirdIcon,
    content: () => <div>Third page</div>,
    order: 3,
    alt: 'alt',
  },
  reports: {
    src: FourthIcon,
    content: Reports,
    order: 4,
    alt: 'reports icon',
  },
  fifthPageMock: {
    src: FifthIcon,
    content: () => <div>Fifth page</div>,
    order: 5,
    alt: 'alt',
  },
};

const HomeScreen = () => {
  const [selectedPage, setSelectedPage] = useState<Pages>('reports');

  const sideMenuPageNames = Object.keys(sideMenuPageOptions);
  const selectedPageData = sideMenuPageOptions[selectedPage];
  const Content = selectedPageData.content;

  return (
    <BaseLayout>
      <div className="grid grid-cols-[auto_1fr] pl-[35px] pr-[100px] pt-[35px] pb-0;">
        <aside className="h-[250px] flex flex-col sticky mr-[45px] top-10">
          {sideMenuPageNames.map((pageName, index) => {
            const pageData = sideMenuPageOptions[pageName] as PageData;
            return (
              <img
                onClick={() => setSelectedPage(pageName as Pages)}
                className={`h-6 w-6 ${index !== 0 ? 'mt-6' : ''} ${
                  selectedPage === pageName
                    ? 'border cursor-pointer border-solid border-[green];'
                    : ''
                }`}
                key={pageName}
                src={pageData.src}
                alt={pageData.alt}
              />
            );
          })}
        </aside>
        <main>
          <Content />
        </main>
      </div>
    </BaseLayout>
  );
};

export default HomeScreen;
