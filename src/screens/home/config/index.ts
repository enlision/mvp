import { Option } from '../types';

export const DEFAULT_PROJECT_OPTION: Option = { value: '', label: 'All projects' };
export const DEFAULT_GATEWAY_OPTION: Option = { value: '', label: 'All gateways' };
