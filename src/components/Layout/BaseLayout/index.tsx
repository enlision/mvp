import React from 'react';
import { Footer, Header } from './components';

type Props = {
  children: React.ReactNode;
};

const BaseLayout: React.FC<Props> = ({ children }) => {
  return (
    <div className="min-h-screen h-full w-screen grid grid-rows-[auto_1fr_auto]">
      <Header />
      {children}
      <Footer />
    </div>
  );
};

export default BaseLayout;
