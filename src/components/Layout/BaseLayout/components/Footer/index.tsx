import React from 'react';

function Footer() {
  return (
    <footer className="font-bold text-base leading-[19px] text-[#005b96] pl-[100px] pr-[22px] py-[22px]">
      Terms&Conditions | Privacy policy
    </footer>
  );
}

export default Footer;
