import React from 'react';

function Header() {
  return (
    <header className="flex justify-between pl-[35px] pr-[100px] py-5 border-b-2 border-b-[#f3f6f9] border-solid">
      <div className="flex items-center">
        <img src="./logo.png" alt="logo" />
        <img className="ml-10" src="./hamburger.png" alt="hamburger" />
      </div>
      <div className="flex items-center ml-auto">
        <div className="w-[43px] h-[43px] flex items-center justify-center rounded-[5px] bg-[#f6ca65]">
          <span className="text-2xl leading-[27px] text-white font-bold">JD</span>
        </div>
        <span className="font-bold text-base leading-[19px] text-[#005b96] ml-[11px]">
          John Doe
        </span>
      </div>
    </header>
  );
}

export default Header;
